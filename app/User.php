<?php

namespace App;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    protected static function boot(){
        parent::boot();
        static::creating(function ($model){
            if( ! $model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return "string";
    }
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function otp_code(){
        return $this->HasOne(Otp_code::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isVerified(){
        if($this->email_verified_at != NULL){
            return true;
        }
        return false;
    }
    public function isAdmin(){
        if($this->roles_id == "0bfefc62-2536-4e28-93e1-b76b089c1113"){
            return true;
        }else{
            return false;
        }

    }



}
