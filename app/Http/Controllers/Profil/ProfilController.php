<?php

namespace App\Http\Controllers\Profil;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\User;

class ProfilController extends Controller
{
    public function show(){
        return response([
           'response code' => "07",
           'response message' => 'Your Profile',
            'data'=> auth()->user()
        ]);
    }
    public function update(Request $request){
        $rules = [
            'name' => ['string','required', 'max:255', 'min:3'],
            'photo' =>['file']
        ];
        $pesan = [
            'required' => 'Nama Harus Diisi',
        ];
        $this->validate($request,$rules,$pesan);
        $file = $request->file('photo');
        $id_user = auth()->user()->id;
		$nama_file = $id_user."_".$file->getClientOriginalName();
        $tujuan_upload = '/photos/user/';
        $alamat_photo = $tujuan_upload . $nama_file;
        $file->move(public_path($tujuan_upload), $nama_file);

        try {
            User::where('id',$id_user)->update([
                'name' => $request['name'],
                'photo' => $alamat_photo
            ]);

            return response([
                'response code' => "07",
                'response message' => 'Your Profile Has Been Updated',
                 'data'=> auth()->user()
             ]);
        } catch (\Exception $e) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'photo profile gagal upload',
                'data' =>$data
            ],200);
        }

    }
}
