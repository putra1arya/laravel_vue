<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PesanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function verifikasi_email(){
        return "Halaman User Sudah Verifikasi Email";
    }
    public function verifikasi_email_dan_admin(){
        return "Halaman Admin yang Sudah Verifikasi Email";
    }
}
