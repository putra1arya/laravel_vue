<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\User;
use App\Otp_code;
use Illuminate\Support\Carbon;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // return 'register';
        $rules = [
            'name' => ['string','required', 'max:255', 'min:3'],
            'email' =>['email', 'required', 'unique:users,email']
        ];
        $pesan = [
            'required' => 'Atribut name dan email harus terisi',
            'unique' => 'Email yang Anda Masukkan Sudah Ada'
        ];
        $this->validate($request,$rules,$pesan);

        do{
            $otp_code = rand(100000,999999);
            $check = Otp_code::where('otp_code', $otp_code)->first();
        }while($check);
            $idUser = User::create([
                'name' => request('name'),
                'email' => request('email'),
            ]);
            $getId = $idUser->id;

            $otp = Otp_code::create([
                'otp_code' => $otp_code,
                'user_id' => $getId,
                'valid_until' => Carbon::now()->addMinutes(5)
            ]);

        event(new UserRegisteredEvent($otp));
        return response([
            'response' => '00',
            'response_message' => 'Silahkan Cek Email',
            'data' => new UserResource(User::find($getId))
            // 'token' => json(compact('token'));
        ]);
    }
}
