<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Events\UserRegisteredEvent;

class VerifyEmailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verifyemail(Request $request)
    {
        $rules = [
            'otp' => ['required']
        ];
        $pesan = [
            'required' => 'OTP Kode Harus Diisi'
        ];
        $this->validate($request,$rules,$pesan);

        $x = Otp_code::where('otp_code', $request['otp'])->first();
        $now = Carbon::now();
        if($now > $x->valid_until ){
            return response([
                'response_code' => '00',
                'response_message' => 'Code OTP Anda Sudah Expired'
            ]);
        }
        if($request['otp']==$x->otp_code){
            User::where('id',$x->user_id)->update([
                'email_verified_at' => Carbon::now()
            ]);
            return response([
                'response_code' => '01',
                'response_message' => 'Akun Telah DiVerifikasi'
            ]);
        }else{
            return response([
                'response_code' => '03',
                'response_message' => 'Kode OTP Salah'
            ]);

        }


    }

    public function getNewCode(Request $request){
        $rules = [
            'email' => ['email','required']
        ];
        $pesan = [
            'required' => 'Email Harus Diisi'
        ];
        $this->validate($request,$rules,$pesan);

        $otp_code = rand(100000,999999);
        $user = User::where('email', $request['email'])->first();
        $x = $user->otp_code;
        Otp_code::where('id',$x->id)->update([
            'otp_code' => $otp_code,
            'valid_until' => Carbon::now()->addMinutes(5)
        ]);
        event(new UserRegisteredEvent($x));
        return response([
            'response code' => '04',
            'response message' => 'Kode OTP Baru Telah dikirim Silahkan Cek Email Anda'
        ]);
    }
}
