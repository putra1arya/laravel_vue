<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UpdatePasswordController extends Controller
{
    public function update(Request $request){
        $rules = [
            'email' =>['email', 'required'],
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation|required',
            'password_confirmation' => ['required','min:6']
        ];
        $pesan = [
            'required' => 'Atribut email, password dan password confirmation harus terisi',
        ];
        $this->validate($request,$rules,$pesan);
        $user = User::where('email',$request['email'])->first();
        User::where('id', $user->id)->update([
            'password' =>bcrypt($request['password'])
        ]);
        return response([
            'response code' => '05',
            'response message' => 'Password Berhasil di Update'
        ]);
    }
}
