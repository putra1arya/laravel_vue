<?php

namespace App\Http\Controllers;

use App\Blogs;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count){
        $blogs = Blogs::select('*')
                ->inRandomOrder()
                ->limit($count)
                ->get();

        $data['blogs'] = $blogs;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data berhasil ditampilkan',
            'data' => $data
        ], 200);
    }
    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        $blog = Blogs::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        $data['blog'] = $blog;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $blog->id . "." . $image_extension;
            $image_folder = '/photos/blog/';
            $image_location = $image_folder . $image_name;

            try{
                $image->move(public_path($image_folder), $image_name);

                $blog->update([
                    'image' => $image_location,
                ]);
            }catch(\Exception $e){
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo blog gagal upload',
                    'data' =>$data
                ],200);
            }
        }
        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditambahkan',
            'data' => $data
        ],200);
    }
}
