<?php

namespace App\Traits;

trait UsesUuid{
    public static function bootUsesUuid(){
        parent::boot();
        static::creating(function ($model){
            if( ! $model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    public function getIncrementing()
    {
        return false;
    }
    public function getKeyType()
    {
        return "string";
    }
}
?>
