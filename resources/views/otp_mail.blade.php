<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OTP-Mail-Verification</title>
</head>
<body>
    <h4>Terima Kasih Telah Bergabung Bersama Kami</h4>
    <h5>Masukkan Kode Berikut Untuk Menverifikasi Akun Anda</h5>
    <h2>{{$otp_code}}</h2>
</body>
</html>
