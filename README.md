Video Demo
https://drive.google.com/file/d/1B6tpflQtno5aUSKyZ3ngdXABXZ1tnWdE/view?usp=sharing

ScreenShot
![detail](/uploads/a1de04e28e5f8de2fffda9d6e44b343f/detail.PNG)

![home](/uploads/8f9120fa9a19c5f2975872453638740c/home.PNG)

![login_with_google](/uploads/8f6fdeb3a3eb7261e8d87f727050f2c1/login_with_google.PNG)

![login_with_google2](/uploads/2b12b1135d56390d0f59876572b7bc74/login_with_google2.PNG)

![search](/uploads/23dbe9d0791b6d513fc28527c9c14481/search.PNG)

![sidebar](/uploads/38490ee816dfa523459f17e3e144abc0/sidebar.PNG)
