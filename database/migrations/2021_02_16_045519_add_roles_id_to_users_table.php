<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRolesIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->uuid('roles_id');
            $table->uuid('otp_code_id');
            $table->foreign('roles_id')->references('id')->on('roles');
            $table->foreign('otp_code_id')->references('id')->on('otp_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['roles_id','otp_code_id']);
            $table->dropColumn(['roles_id','otp_code_id']);
        });
    }
}
