<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropOtpIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->foreign('otp_code_id')->onDelete('cascade')->change();
            $table->dropForeign(['otp_code_id']);
            $table->dropColumn(['otp_code_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->uuid('otp_code_id');
            $table->foreign('otp_code_id')->references('id')->on('otp_codes');
            // $table->string('otp_code_id',36)->change();
        });
    }
}
