<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => "AdminGanteng",
            'email' => "admin@ganteng.com",
            'password' => Hash::make("12341234"),
            'roles_id' => "0bfefc62-2536-4e28-93e1-b76b089c1113",
            'email_verified_at' => date("Y-m-d\TH:i:s")
        ]);
        App\User::create([
            'name' => "EliteMiko",
            'email' => "miko@elite.com",
            'password' => Hash::make("12341234"),
            'roles_id' => "113fc8eb-dcbd-4049-a04b-61a747fee334",
            'email_verified_at' => date("Y-m-d\TH:i:s")
        ]);
    }
}
