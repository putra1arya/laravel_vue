<?php

use Illuminate\Database\Seeder;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Role::create([
            'nama' => 'user'
        ]);
        App\Role::create([
            'nama' => 'admin'
        ]);
    }
}
