<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{any?}', function(){
//     return 'masuk ke sini';
// })->where('any', '.*');

Route::view('/{any?}', 'app')->where('any', '.*');
// Route::middleware(['admin', 'verifikasi_email'])->group(function () {
//     Route::get('/route-2', 'PesanController@verifikasi_email_dan_admin');
// });
// Route::get('/route-1','PesanController@verifikasi_email')->middleware('verifikasi_email');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
