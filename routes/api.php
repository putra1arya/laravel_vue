<?php

Route::namespace('Auth')->group(function () {
    Route::post('auth/register', 'RegisterController');
    Route::post('auth/verifikasi', 'VerifyEmailController@verifyemail');
    Route::post('auth/regenerate-OTP', 'VerifyEmailController@getNewCode');
    Route::post('auth/update_password', 'UpdatePasswordController@update');
    Route::post('auth/login', 'LoginController');
    Route::post('auth/logout', 'LogoutController')->middleware('auth:api');
    Route::post('auth/check-token', 'CheckTokenController')->middleware('auth:api');

    Route::get('auth/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('auth/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
});
Route::namespace('Profil')->middleware(['auth:api','verifikasi_email'])->group(function () {
    Route::get('profil/get_profil', 'ProfilController@show');
    Route::post('profil/edit_profil', 'ProfilController@update');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaign',
], function(){
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/','CampaignController@index');
    Route::get('/{id}', 'CampaignController@detail');
    Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blog',
], function(){
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});
